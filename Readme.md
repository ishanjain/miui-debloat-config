This is a configuration file to used by the debloat magisk module. It is intended to be used on MIUI based roms.

I created it because all the configurations I could find online were in two extremes.

1. Didn't remove enough bloat or removed a different kind of bloat
2. Removed everything! Including the dialer, Messaging app, Literally everything.

Both of these were inconvenient for me. So, When I reinstalled MIUI ROM on my phone
I took a bit more time to try out what apps were installed on my phone and which ones I could remove without breaking everything.
So, This is basically a list of those apps.

The reason for putting this on git is,

1. I can go back to an older revision if something starts breaking on my phone because some thing can't find an app.
2. An online backup of my list since it was time consuming to get a decent list in the first place.


